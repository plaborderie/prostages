FROM php:7.3-rc-cli
COPY src/ /var/application

WORKDIR /var/application

RUN apt-get clean
RUN apt-get update

# Install some basic tools
RUN apt-get install -y \
  git \
  tree \
  vim \
  wget \
  subversion

# Install some base extensions
RUN apt-get install -y \
  libzip-dev \
  zlib1g-dev \
  zip \
  && docker-php-ext-install zip

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

# Install node, npm and yarn
RUN apt-get install -y gnupg
RUN wget -qO- https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

# Install yarn
RUN npm install -g yarn

# Run the Symfony server
CMD [ "./bin/console", "server:run", "0.0.0.0:3000"]