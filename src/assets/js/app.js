/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require("../css/global.scss");

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require("jquery");
require("bootstrap");

// Changing class on click navbar button (mobile only)
$(".navbar-toggler").click(function() {
  // $(".navbar-toggler").removeClass("untoggled");
  $(this).hasClass("toggled")
    ? $(this).removeClass("toggled")
    : $(this).addClass("toggled");
  // $(this).addClass("toggled");
});

console.log("Hello Webpack Encore! Edit me in assets/js/app.js");
