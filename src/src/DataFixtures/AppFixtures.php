<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Entreprise;
use App\Entity\Formation;
use App\Entity\Stage;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Generating fake data
        $faker = \Faker\Factory::create('fr_FR');

        // Generating formation
        $formation = new Formation();
        $formation->setNom("DUT Informatique");

        for ($i = 0; $i < 20 ; $i++) {
            // Generating entreprises

            $entreprise = new Entreprise();
            $entreprise->setNom($faker->company);
            $entreprise->setActivite($faker->catchPhrase);
            $entreprise->setAdresse($faker->address);
            $entreprise->setSite($faker->url);
            $manager->persist($entreprise);

            // Generating stages

            $stage = new Stage();
            $stage->setNom($faker->jobTitle);
            $stage->setDescription($faker->realText(500, 2));
            $stage->setEmail($faker->email);
            $stage->setTel($faker->e164PhoneNumber);
            $stage->setContact($faker->name);
            $stage->setEntreprise($entreprise);
            $manager->persist($stage);

            // Adding stage to formation
            $formation->addStage($stage);
            $manager->persist($formation);

        }

        $manager->flush();
    }
}
