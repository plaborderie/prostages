<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Entity\Formation;
use App\Entity\Stage;

use App\Repository\EntrepriseRepository;
use App\Repository\FormationRepository;
use App\Repository\StageRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;


class ProstagesController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('prostages/index.html.twig');
    }

    /**
     * @Route("/entreprises", name="entreprises")
     */
    public function entreprises(EntrepriseRepository $repEntreprises) {
        $entreprises = $repEntreprises->findAll();
        return $this->render('prostages/entreprises.html.twig', ['entreprises' => $entreprises]);
    }

    /**
     * @Route("/formations", name="formations")
     */
    public function formations(FormationRepository $repFormations) {
        $formations = $repFormations->findAll();
        return $this->render('prostages/formations.html.twig', ['formations' => $formations]);
    }

    /**
     * @Route("/stages", name="stages")
     */
    public function stages(StageRepository $repStages) {
        $stages = $repStages->findAll();
        return $this->render('prostages/stages.html.twig', ['stages' => $stages]);
    }

    /**
     * @Route("/stages/{id}", name="stage")
     */
    public function stage(Stage $stage) {
        // $stage = $repStages->findOneBy(['id' => $id]);
        return $this->render('prostages/stage.html.twig', [
            'stage' => $stage
        ]);
    }

    /**
     * @Route("/entreprises/{id}", name="entreprise")
     */
    public function entreprise(Entreprise $entreprise) {
        return $this->render('prostages/entreprise.html.twig', [
            'entreprise' => $entreprise
        ]);
    }

    /**
     * @Route("/formations/{id}", name="formation")
     */
    public function formation(Formation $formation) {
        return $this->render('prostages/formation.html.twig', [
            'formation' => $formation
        ]);
    }


}
