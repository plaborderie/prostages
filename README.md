# Comment lancer le projet symfony ?

- Avoir docker installé et lancé sur sa machine
- Depuis sa machine, faire :
  - `cd src`
  - `composer install` pour installer les packages PHP
  - `npm install` pour installer les modules javascript (bootstrap, jQuery, popper.js)
  - Si yarn n'est pas installé, `npm install -g yarn`
  - `yarn encore dev` pour build le css/js (Bootstrap)
- Ensuite, depuis la racine, lancer `docker-compose up`
- Lancer le navigateur sur `localhost:3000`
